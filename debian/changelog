systemd-bootchart (233-2) unstable; urgency=medium

  [ Michael Biebl ]
  * Update Vcs-* to point to https://salsa.debian.org
  * Set Rules-Requires-Root to no

  [ Nicolas Braud-Santoni ]
  * debian/copyright: Point to CC-1.0 license in /usr/share/common-licenses/
    (Closes: #882630)

  [ Martin Pitt ]
  * debian/copyright: Use https URL for Format:
  * Bump Standards-Version to 4.1.3
  * debian/copyright: Move global wildcard section to the top.
    Fixes lintian warning
    global-files-wildcard-not-first-paragraph-in-dep5-copyright.

 -- Martin Pitt <mpitt@debian.org>  Sun, 25 Mar 2018 23:53:27 +0200

systemd-bootchart (233-1) unstable; urgency=medium

  * New upstream version 233
  * Drop debian/patches/Revert-bootchart-fix-per-cpu-small-scales.patch
  * Bump Standards-Version to 4.1.0

 -- Michael Biebl <biebl@debian.org>  Sat, 16 Sep 2017 19:12:17 +0200

systemd-bootchart (232-1) unstable; urgency=medium

  [ Michael Biebl ]
  * New upstream version 232
  * Bump debhelper compat level to 10 for automatic dh-autoreconf
  * Update Vcs-* following the latest recommendation
  * Revert "bootchart: fix per-cpu & small scales."

  [ Felipe Sateler ]
  * Do not enable systemd-bootchart.service by default

 -- Michael Biebl <biebl@debian.org>  Sun, 18 Jun 2017 23:44:18 +0200

systemd-bootchart (231-1) unstable; urgency=medium

  * New upstream release.
  * Use the git archive (gzipped) instead of the release tarball. The latter
    is compressed using xz and causes failures with pristine-tar.

 -- Michael Biebl <biebl@debian.org>  Wed, 14 Sep 2016 22:36:25 +0200

systemd-bootchart (230-2) unstable; urgency=medium

  * Fix paths in man pages
  * Install upstream README which documents required kernel interfaces

 -- Michael Biebl <biebl@debian.org>  Sun, 03 Jul 2016 14:52:06 +0200

systemd-bootchart (230-1) unstable; urgency=medium

  * Initial release. This got split off from the main systemd sources in
    version 230.

 -- Martin Pitt <mpitt@debian.org>  Wed, 22 Jun 2016 15:13:08 +0200
